package io.vimero.earthquake;

import io.vimero.api.controller.MovementController;
import io.vimero.api.data.MovementData;
import io.vimero.entity.Movement;
import io.vimero.repository.MovementRepository;
import io.vimero.repository.MovementRxRepository;
import io.vimero.service.MovementService;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = MovementController.class)
@Import(MovementService.class)
public class MovementControllerTest {

    @Autowired
    private ApplicationContext applicationContext;


    @MockBean
    private MovementRxRepository movementRxRepository;

    @MockBean
    private MovementRepository movementRepository;

    @Autowired
    private WebTestClient webClient;

    @Test
    void create() {
        MovementData data = new MovementData();
        webClient.post()
                .uri("/v1/earthquake/movements")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(data))
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void list() {
        webClient.get()
                .uri("/v1/earthquake/movements")
                .header(HttpHeaders.ACCEPT, "application/json")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Movement.class);
    }

    @Test
    void find() {
        webClient.get()
                .uri(uriBuilder -> uriBuilder.path("/v1/earthquake/movements/date/2019-12-13")
                                .queryParam("radius", "0")
                                .queryParam("latitude", "1222")
                                .queryParam("longitude", "2222")

                        .build())
                .header(HttpHeaders.ACCEPT, "application/json")
                .exchange()
                .expectStatus().isOk();
    }

}
