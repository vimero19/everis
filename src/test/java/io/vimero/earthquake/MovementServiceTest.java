package io.vimero.earthquake;

import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import io.vimero.api.data.GeoData;
import io.vimero.api.resource.IntensityResource;
import io.vimero.api.resource.MovementResource;
import io.vimero.bean.DurationBean;
import io.vimero.entity.Movement;
import io.vimero.repository.MovementRepository;
import io.vimero.service.MovementService;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;
import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class MovementServiceTest {

    private static final double LIMA_LATITUDE = -12.0431805;
    private static final double LIMA_LONGITUDE = -77.0282364;

    @Rule
    public MongoDbRule mongoDbRule = newMongoDbRule().defaultSpringMongoDb("demo-test");

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MovementRepository movementRepository;

    @Autowired
    private MovementService movementService;

    @Test
    public void findByDate() {

        Movement lima = getMovement(LIMA_LATITUDE, LIMA_LONGITUDE);
        Movement trujillo =  getMovement(-8.1159897, -79.0299835);
        movementRepository.save(lima);
        movementRepository.save(trujillo);

        DurationBean<IntensityResource> bean = movementService.find(LocalDate.now().toString(), 500, LIMA_LATITUDE, LIMA_LONGITUDE);

        List<MovementResource> list = bean.getResource().getEvents();
        long total = list.size();

        assertThat(total).isEqualTo(2);
    }

    @Test
    public void findByDate2() {

        Movement lima = getMovement(LIMA_LATITUDE, LIMA_LONGITUDE);
        Movement trujillo =  getMovement(-8.1159897, -79.0299835);
        movementRepository.save(lima);
        movementRepository.save(trujillo);

        DurationBean<IntensityResource> bean = movementService.find("2019-12-10", 500, LIMA_LATITUDE, LIMA_LONGITUDE);

        assertThat(bean.getResource().getEvents()).isNull();
    }

    private Movement getMovement(double latitude, double longitude){
        GeoData geo = new GeoData();
        geo.setLatitude(latitude);
        geo.setLongitude(longitude);
        Movement movement = new Movement();
        movement.setId(UUID.randomUUID().toString());
        movement.setGeo(geo);
        movement.setIntensity(100);

        return movement;
    }

}