package io.vimero.component;

import io.vimero.type.Header;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
public class ResponseHeaderWebFilter  implements WebFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        try {
            exchange.getResponse()
                    .getHeaders()
                    .add(Header.HOST, InetAddress.getLocalHost().getHostAddress());
        }catch (UnknownHostException e){
            e.printStackTrace();
        }

        return chain.filter(exchange);
    }
}
