package io.vimero.service;

import io.vimero.api.data.GeoData;
import io.vimero.api.data.MovementData;
import io.vimero.api.resource.IntensityResource;
import io.vimero.api.resource.MovementResource;
import io.vimero.bean.DurationBean;
import io.vimero.entity.Movement;
import io.vimero.repository.MovementRepository;
import io.vimero.repository.MovementRxRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class MovementService {

    private final static int EARTH_RADIUS = 6371;

    private MovementRxRepository movementRxRepository;
    private MovementRepository movementRepository;

    public MovementService(MovementRxRepository movementRxRepository, MovementRepository movementRepository) {
        this.movementRxRepository = movementRxRepository;
        this.movementRepository = movementRepository;
    }

    public Flux<Movement> findAll(){
        return movementRxRepository.findAll();
    }

    public DurationBean<Mono<Movement>> save(MovementData data){
        LocalDateTime dateInit =  LocalDateTime.now();
        String id = UUID.randomUUID().toString();
        Movement movement = new Movement();
        movement.setId(id);
        movement.setDeepness(data.getDeepness());
        movement.setIntensity(data.getIntensity());
        movement.setGeo(data.getGeo());
        Mono<Movement> entity = movementRxRepository.save(movement);

        return create(entity, dateInit);
    }

    public DurationBean<IntensityResource> find(String date, int radius, double latitude, double longitude){
        LocalDateTime dateInit =  LocalDateTime.now();
        IntensityResource resource = new IntensityResource();
        try{
            Optional<List<Movement>> movements = movementRepository.findByDate(LocalDate.parse(date));
            if( movements.isPresent() ){
                List<Movement> filter= movements.get()
                        .stream().filter(movement -> getDistance(latitude, longitude, movement.getGeo() ) <= radius)
                        .collect(toList());
                if( !filter.isEmpty() ){
                    List<MovementResource> events = filter.stream()
                            .map(movement -> toDTO(movement)).collect(toList());
                    Movement max = filter.stream().max(Comparator.comparing(Movement::getIntensity))
                            .orElseThrow(NoSuchElementException::new);
                    Movement min = filter.stream().min(Comparator.comparing(Movement::getIntensity))
                            .orElseThrow(NoSuchElementException::new);

                    resource.setEvents(events);
                    resource.setMinIntensity(min.getIntensity());
                    resource.setMaxIntensity(max.getIntensity());
                }else{
                    return create(resource, dateInit);

                }
            }else{
                return create(resource, dateInit);

            }


        }finally {
            return create(resource, dateInit);
        }

    }

    private String getTime(LocalDateTime  dateInit){
        Duration duration = Duration.between(LocalDateTime.now(), dateInit);

        return String.valueOf(Math.abs(duration.getSeconds()));
    }

    private <T> DurationBean<T> create(T resource, LocalDateTime dateInit){
        DurationBean<T> bean =  new DurationBean<>();
        bean.setResource(resource);
        bean.setDuration(getTime(dateInit));

        return bean;
    }

    private MovementResource toDTO(Movement movement){
        return new MovementResource(movement.getIntensity(), movement.getGeo());
    }

    private double getDistance(double latitude, double longitude, GeoData geo){
        double distanceLatitude = Math.toRadians(geo.getLatitude() - latitude);
        double distanceLongitude = Math.toRadians(geo.getLongitude() - longitude);
        double sinLatitude = Math.sin(distanceLatitude / 2);
        double sinLongitude = Math.sin(distanceLongitude / 2);
        double pow = Math.pow(sinLatitude, 2) + Math.pow(sinLongitude, 2)
                * Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(longitude));
        double atan2 = 2 * Math.atan2(Math.sqrt(pow), Math.sqrt(1 - pow));
        double distance = EARTH_RADIUS * atan2;

        return distance;
    }

}
