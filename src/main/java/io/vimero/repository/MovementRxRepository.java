package io.vimero.repository;

import io.vimero.entity.Movement;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface MovementRxRepository extends ReactiveMongoRepository<Movement, String> {

}
