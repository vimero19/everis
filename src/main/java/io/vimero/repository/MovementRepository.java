package io.vimero.repository;

import io.vimero.entity.Movement;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovementRepository extends MongoRepository<Movement, String> {

    Optional<List<Movement>> findByDate(LocalDate date);

}
