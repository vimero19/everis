package io.vimero.api.controller;

import io.vimero.api.data.MovementData;
import io.vimero.api.resource.IntensityResource;
import io.vimero.bean.DurationBean;
import io.vimero.entity.Movement;
import io.vimero.service.MovementService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import static io.vimero.type.Header.DURATION;

@RestController
@RequestMapping("v1/earthquake/movements")
public class MovementController {

    private MovementService movementService;

    public MovementController(MovementService movementService) {
        this.movementService = movementService;
    }

    @GetMapping
    public Flux<Movement> getMovements() {
        return movementService.findAll();
    }

    @GetMapping("date/{date}")
    public Mono<ResponseEntity> getMovementsByIntensity(@PathVariable String date,
                                                        @RequestParam double latitude,
                                                        @RequestParam double longitude,
                                                        @RequestParam int radius) {
        DurationBean<IntensityResource> result =  movementService.find(date, radius, latitude, longitude);
        return Mono.just(ResponseEntity.ok()
                .header(DURATION, result.getDuration())
                .body(result.getResource()));
    }

    @PostMapping
    public Mono<ResponseEntity> createMovement(@Valid @RequestBody MovementData movement) {
        DurationBean<Mono<Movement>> result = movementService.save(movement);
        return Mono.just(ResponseEntity.ok()
                .header(DURATION, result.getDuration())
                .body(result.getResource()));
    }

}
