package io.vimero.api.resource;

import io.vimero.api.data.GeoData;

import java.io.Serializable;

public class MovementResource implements Serializable {

    private Integer intensity;
    private GeoData geo;

    public MovementResource(Integer intensity, GeoData geo) {
        this.intensity = intensity;
        this.geo = geo;
    }

    public Integer getIntensity() {
        return intensity;
    }

    public void setIntensity(Integer intensity) {
        this.intensity = intensity;
    }

    public GeoData getGeo() {
        return geo;
    }

    public void setGeo(GeoData geo) {
        this.geo = geo;
    }
}
