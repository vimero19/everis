package io.vimero.api.resource;

import java.util.List;

public class IntensityResource {

    private Integer maxIntensity;
    private Integer minIntensity;
    private List<MovementResource> events;

    public Integer getMaxIntensity() {
        return maxIntensity;
    }

    public void setMaxIntensity(Integer maxIntensity) {
        this.maxIntensity = maxIntensity;
    }

    public Integer getMinIntensity() {
        return minIntensity;
    }

    public void setMinIntensity(Integer minIntensity) {
        this.minIntensity = minIntensity;
    }

    public List<MovementResource> getEvents() {
        return events;
    }

    public void setEvents(List<MovementResource> events) {
        this.events = events;
    }

}
