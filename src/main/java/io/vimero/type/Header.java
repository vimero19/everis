package io.vimero.type;

public interface Header {

    String HOST = "X-Host";
    String DURATION = "X-Duration";

}
