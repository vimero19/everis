package io.vimero.entity;

import io.vimero.api.data.GeoData;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document(collection = "movements")
public class Movement {

    @Id
    private String id;
    private Integer intensity;
    private Integer deepness;
    private LocalDate date;
    private GeoData geo;

    public Movement() {
        date = LocalDate.now();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIntensity() {
        return intensity;
    }

    public void setIntensity(Integer intensity) {
        this.intensity = intensity;
    }

    public Integer getDeepness() {
        return deepness;
    }

    public void setDeepness(Integer deepness) {
        this.deepness = deepness;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public GeoData getGeo() {
        return geo;
    }

    public void setGeo(GeoData geo) {
        this.geo = geo;
    }
}
